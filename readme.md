# KubeCon/CloudNativeCon EU 2023 Code Challenge

## Getting started

Welcome to the KubeCon/CloudNativeCon EU 2023 Code Challenge hosted on CodeChallenge.dev. _We're so glad you're here_.

Fork this project into your namespace to get started.

## Level 1

This project provides an Image-to-ASCII art converter in Golang. The CI/CD jobs can generate [SLSA-2 attestation](https://about.gitlab.com/blog/2022/11/30/achieve-slsa-level-2-compliance-with-gitlab/) for job artifacts to help achieve SLSA Level 2 compliance with GitLab. Edit the `.gitlab-ci.yml` file and specify `RUNNER_GENERATE_ARTIFACTS_METADATA: 1` in the `variables` section. Commit and create a Merge Request. Navigate into the `build` job details, and browse the artifacts to inspect the `artifacts-metadata.json` SLSA attestation file.

The level will be completed once the `RUNNER_GENERATE_ARTIFACTS_METADATA` variable will be detected in `main.go` in the MR changes. Note: The pipeline fails still, continue with level 2 to fix it.

## Level 2

For level 2, the Image-to-ASCII art converter CI/CD tests need to be fixed. No worries, there is no deep Golang knowledge required. The tests fail and block the CI/CD pipeline deployment, hindering you to see the beautiful ASCII art. Tip: Someone forgot to update the challenge from last year in `main.go`.

Fix the wrong value in the `helloFrom` variable, commit the changes and inspect the `deploy` job in the CI/CD pipeline. As a bonus exercise, change the `IMAGE_PATH` variable in `.gitlab-ci.yml` to the proposed value, and see what happens.

The level will be completed once the corrected value will be detected in `main.go` in the MR changes.

## Level 3

For level 3, visit the [Contributing to GitLab](https://about.gitlab.com/community/contribute/) page to get started. There are many things to contribute to:

* The Ruby on Rails backend
* The VueJS-based frontend
* The Go-based services like the GitLab Runner and Gitaly
* The Go-based GitLab CLI
* The Terraform provider for GitLab
* The cloud-native installation (Operator, Helm charts)
* The documentation for everything
* Translations
* And more!

You can also participate in one of our upcoming [Hackathons](https://about.gitlab.com/community/hackathon/#sessions).
